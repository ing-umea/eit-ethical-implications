# EIT Ethical Implications


## See also

* [Project Homepage][project-homepage]

* [Humane AI Net page][humane-ai-page]

* **Publication:** <a id="AlBaKoMe2022" /> Andrea Aler Tubella, [Flavia Barsotti][gitlab-flbarsotti], [Rüya Gökhan Koçer][gitlab-rugoko], and [Julian Alfredo Mendez][gitlab-julianmendez]: **Ethical implications of fairness interventions: what might be hidden behind engineering choices?**. *Ethics and Information Technology*, volume 24, issue 1, article 12, Springer 2022.
 &nbsp; DOI:[10.1007/s10676-022-09636-z][paper-doi]
 &nbsp; [Abstract][paper-abstract]
 &nbsp; [BibTeX][paper-bibtex]
 &nbsp; [PDF][paper-pdf]

[project-homepage]: https://www.ai4europe.eu/research/research-bundles/validating-fairness-property-post-processing-vs-processing-systems
[humane-ai-page]: https://www.humane-ai.eu/project/tmp-128/
[gitlab-flbarsotti]: https://gitlab.com/fl.barsotti
[gitlab-rugoko]: https://gitlab.com/rugoko
[gitlab-julianmendez]: https://gitlab.com/julianmendez
[paper-doi]: https://doi.org/10.1007/s10676-022-09636-z
[paper-abstract]: https://link.springer.com/article/10.1007/s10676-022-09636-z#Abs1
[paper-bibtex]: https://citation-needed.springer.com/v2/references/10.1007/s10676-022-09636-z?format=bibtex
[paper-pdf]: https://link.springer.com/content/pdf/10.1007/s10676-022-09636-z.pdf


